# Authors

## Current autoMEA maintainers

- Vinicius Hernandes

## Other contributors

- Anouk M. Heuvelmans
- Valentina Gualtieri
- Dimphna H. Meijer
- Geeske M. van Woerden
- Eliska Greplova

## Funding

This project was supported by KIND Synergy Grant from Kavli Institute of Nanoscience and the Dutch Research Council (NWO) Vidi Grant (016.Vidi.188014). This publication is part of the project Engineered Topological Quantum Networks (with Project No. VI.Veni.212.278) of the research
program NWO Talent Programme Veni Science domain 2021 which is financed by the Dutch Research Council (NWO), and by the Dutch TSC Foundation (STSN).
