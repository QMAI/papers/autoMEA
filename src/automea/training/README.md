
### This folder contains all the scripts used to train and evaluate the machine learning models for autoMEA. 

The data can be downloaded from https://zenodo.org/records/12685150

The MEA datasets used to train the machine learning models are saved in 'data/datasets/'. The datasets used to validate the models, and compare their results with manual analysis are saved in 'data/datasets_validation/'.

The following steps can be followed to reproduce the training and evaluation of the machine learning models used to predict MaxInterval parameters.

1. select_5sec_windows.ipynb
    - Program used to analyze 5-second windows with signal, spikes, and selected max interval parameters 
    - Input: '.h5' MEA datasets in 'data/datasets/'
    - Output: 'trainset_info.dat' file containing information about training data (points to h5 dataset files with recorded signal), saved in 'data/'
2. generate_trainset.py
    - Generates a trainset file with 5-second slices of the signal (as a float array), and corresponding spikes and bursts (binary array)
    - Input: 'data/trainset_info.dat'
    - Output:
        - 'trainset.hdf5' file containing slices with signal, spikes, and bursts
        - 'data_channel.hdf5' file containing signal of channels used in trainset
3. split_trainset.py
    - Splits the full training data into training, validation, and test sets, with spikes and signal arrays being the moving average of the original slices
    - Input: 'data/trainset_info.dat', 'trainset.hdf5'
    - Output:
        - 'training_indices.hdf5' file with the indices of 'trainset.hdf5' that are part of the training/validation/test set
        - train, validation, and test set '.hdf5' files stored in 'data/spikes/' for the spike30 model and in 'data/signal/' for signal30 and signal100 models
4. train_models.py
    - Define and train machine learning models that predict max interval parameters
    - Input: 'data/trainset.hdf5', 'data/training_indices.hdf5', training and validation sets in 'data/spikes/' and 'data/signal/'
    - Output:
        - '.h5' files containing trained models, stored in 'models/'
        - '.csv' files containing custom accuracy and loss for each training epoch, for all models, saved in 'data/custom_accuracy/'
5. plot_custom_accuracy.py
    - Generates plot with custom accuracy metric
    - Input: '.csv' files in 'data/custom_accuracy/'
    - Output: plot of custom accuracy vs epoch 
6. generate_pred_vs_def_bursts.py
    - Generates a 5-second window with signal, spikes, and bursts detected using both the machine learning approach, and by using default Max Interval parameters
    - Input:
        - trained machine learning models '.h5' files from 'models/'
        - 'trainset.hdf5', 'trainset_info.dat', and 'data_channel/hdf5' from 'data/'
        - test sets saved in 'data/spikes/' and 'data/signal/'
    - Output: '.png' images with 5-second slices with signal, spikes, and bursts detect using the machine learning model method, and the default max interval parameters, stored in 'data/burst_quality/images/'
7. GUI.py
    - Graphical Interface used to generate burst_quality metric
    - Input: images in 'data/burst_quality/images/'
    - Output: 'burst_quality.txt' file containing the burst quality metric value, in 'GUI/'
8. plot_burst_quality.py
    - Generates plot with histogram showing burst_quality metric
    - Input: 'GUI/burst_quality.txt'
    - Output: histogram plot of burst quality metric