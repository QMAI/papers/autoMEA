# Package reference

### *class* automea._\_main_\_.Analysis

Bases: [`object`](https://docs.python.org/3/library/functions.html#object)

Class for performing analysis of MEA datasets.

This class provides methods for loading datasets, performing various analyses,
and visualizing the results.

#### util

Utility functions module.

* **Type:**
  module

#### dataset

The dataset. Default is an empty list.

* **Type:**
  [list](https://docs.python.org/3/library/stdtypes.html#list)

#### dataset_filename

The filename of the dataset. Default is an empty string.

* **Type:**
  [str](https://docs.python.org/3/library/stdtypes.html#str)

#### dataset_index

The index of the dataset. Default is None.

* **Type:**
  [int](https://docs.python.org/3/library/functions.html#int) or None

#### path_to_dataset

The path to the dataset. Default is an empty string.

* **Type:**
  [str](https://docs.python.org/3/library/stdtypes.html#str)

#### path_to_csv

The path to the CSV file. Default is an empty string.

* **Type:**
  [str](https://docs.python.org/3/library/stdtypes.html#str)

#### path_to_model

The path to the model file. Default is an empty string.

* **Type:**
  [str](https://docs.python.org/3/library/stdtypes.html#str)

#### output_name

The name of the output. Default is an empty string.

* **Type:**
  [str](https://docs.python.org/3/library/stdtypes.html#str)

#### output_folder

The output folder. Default is an empty string.

* **Type:**
  [str](https://docs.python.org/3/library/stdtypes.html#str)

#### wellsIDs

The IDs of the wells. Default is an empty list.

* **Type:**
  [list](https://docs.python.org/3/library/stdtypes.html#list)

#### wellsLabels

The labels of the wells. Default is an empty list.

* **Type:**
  [list](https://docs.python.org/3/library/stdtypes.html#list)

#### well

The selected well. Default is None.

* **Type:**
  [str](https://docs.python.org/3/library/stdtypes.html#str) or None

#### model

The ML model. Default is None.

* **Type:**
  [object](https://docs.python.org/3/library/functions.html#object) or None

#### model_name

The name of the model. Default is None.

* **Type:**
  [str](https://docs.python.org/3/library/stdtypes.html#str) or None

#### signal

The recorded signal. Default is None.

* **Type:**
  array_like or None

#### time

The time array. Default is None.

* **Type:**
  array_like or None

#### spikes

The detected spikes. List with timestamp of every spike. Default is None.

* **Type:**
  array_like or None

#### spikes_binary

The binary representation of detected spikes. Default is None.

* **Type:**
  array_like or None

#### reverbs

The detected reverberations. List of lists with start and end timestamp of reverb. Default is None.

* **Type:**
  array_like or None

#### reverbs_binary

The binary representation of detected reverberations. Default is None.

* **Type:**
  array_like or None

#### bursts

The detected bursts. List of lists with start and end timestamp of burst. Default is None.

* **Type:**
  array_like or None

#### bursts_binary

The binary representation of detected bursts. Default is None.

* **Type:**
  array_like or None

#### net_reverbs

The detected network reverberations. List of lists with start and end timestamp of network reverb. Default is None.

* **Type:**
  array_like or None

#### reverbs_binary

The binary representation of detected network reverberations. Default is None.

* **Type:**
  array_like or None

#### net_bursts

The detected network bursts. List of lists with start and end timestamp of network burst. Default is None.

* **Type:**
  array_like or None

#### net_bursts_binary

The binary representation of detected network bursts. Default is None.

* **Type:**
  array_like or None

#### adZero

Variable to convert integer signal (default in h5 dataset) to mV. Is subracted from integer signal. Default is 0

* **Type:**
  [int](https://docs.python.org/3/library/functions.html#int)

#### conversionFactor

Variable to convert integer signal (default in h5 dataset) to mV. Is multiplied to integer signal. Default is 59604

* **Type:**
  [int](https://docs.python.org/3/library/functions.html#int)

#### exponent

Variable to convert integer signal (default in h5 dataset) to mV. Is multiplied to integer signal. Default is -12

* **Type:**
  [int](https://docs.python.org/3/library/functions.html#int)

#### samplingFreq

Sampling frequency used to measure the signal. Default is 10_000 (10 kHz)

* **Type:**
  [int](https://docs.python.org/3/library/functions.html#int)

#### total_timesteps_signal

Number of timesteps present in measured signal. Default is 6 million.

* **Type:**
  [int](https://docs.python.org/3/library/functions.html#int)

#### time

Array containing time for measured signal in seconds. Uses samplingFreq and total_timesteps_signal.

* **Type:**
  array_like

#### threshold_params

Dictionary of parameters used to detect threshold. Includes:
: ‘rising’
  : How many standard deviations are used to set threshold. Default is 5.
  <br/>
  ‘startTime’
  : Start time (s) to consider signal. Default is 0.
  <br/>
  ‘baseTime’
  : Time lenght in ms to consider signal for std calculation. Default is 0.250.
  <br/>
  ‘segments’
  : Number of segment to perform for calculation. Default is 10.

* **Type:**
  [dict](https://docs.python.org/3/library/stdtypes.html#dict)

#### spike_params

Dictionary of parameters used to detect spikes. Includes:
: - ‘deadtime’: (key
    : Amount of time after detecting a spike for which no spike is detect. Default is 3_000\*1e-6.

* **Type:**
  [dict](https://docs.python.org/3/library/stdtypes.html#dict)

#### reverbs_params

Dictionary of parameters used to detect reverberations. Includes:
: ‘max_interval_start’
  : Maximum interval between spikes to start reverberation detection in ms. Default is 15.
  <br/>
  ‘max_interval_end’
  : Maximum interval between spikes to end reverberation detection in ms. Default is 20.
  <br/>
  ‘min_interval_between’
  : Minimum interval between spikes to consider for reverberation detection in ms. Default is 25.
  <br/>
  ‘min_duration’
  : Minimum reverberation duration in ms. Default is 20.
  <br/>
  ‘min_spikes’
  : Minimum number of spikes to consider in a reverberation. Default is 5.

* **Type:**
  [dict](https://docs.python.org/3/library/stdtypes.html#dict)

#### self.bursts_params

Dictionary of parameters used to detect bursts (from reverberations). Includes:
: ‘min_interval_between’
  : Minimum interval between spikes to consider for reverberation detection in ms. Default is 300.

* **Type:**
  [dict](https://docs.python.org/3/library/stdtypes.html#dict)

#### \_pretrained_models

List containing name of pretrained ML models used for reverberation detection.

* **Type:**
  [list](https://docs.python.org/3/library/stdtypes.html#list) of [str](https://docs.python.org/3/library/stdtypes.html#str)

#### model_params

Dictionary of parameters used in model-based reverberation detection. Includes:
: ‘name’
  : Name of the model. Default is None.
  <br/>
  ‘input_type’
  : String defining type of input used by the model (‘signal’ or ‘spikes’). Deault is None and changed when model is loaded.
  <br/>
  ‘input_average’
  : How many points are used to calculate average of input. Default is 30.
  <br/>
  ‘window_size’
  : Size of window (in timestamps) used as input for the model (before averaging). Default is 50_000.
  <br/>
  ‘window_overlap’
  : Overlap between windows when sweeping a channel. Default is 25_000.

* **Type:**
  [dict](https://docs.python.org/3/library/stdtypes.html#dict)

#### analysis_params

Parameters for analysis - which quantities user wants to save - each one creates an output file. Includes:
: ‘save_spikes’
  : Whether or not to save spikes in a dedicated file. Default is False.
  <br/>
  ‘save_reverbs’
  : Whether or not to save reverberations in a dedicated file. Default is False.
  <br/>
  ‘save_bursts’
  : Whether or not to save bursts in a dedicated file. Default is False.
  <br/>
  ‘save_net_reverbs
  : Whether or not to save network reverberations in a dedicated file. Default is False.
  <br/>
  ‘save_net_bursts’
  : Whether or not to save network bursts in a dedicated file. Default is False.
  <br/>
  ‘save_stats’
  : Whether or not to save statistics in a dedicated file. Default is False.

* **Type:**
  [dict](https://docs.python.org/3/library/stdtypes.html#dict)

#### wellIndexLabelDict

Dictionary to convert well-index to well-label.

* **Type:**
  [dict](https://docs.python.org/3/library/stdtypes.html#dict)

#### wellLabelIndexDict

Dictionary to convert well-label to well-index.

* **Type:**
  [dict](https://docs.python.org/3/library/stdtypes.html#dict)

#### files_and_well_csv(file)

Load filenames and associated wells from a CSV file.

#### loadmodel(setting='default')

Load a machine learning model to perform reverberations/bursts detection.

#### loadsignal(signal)

Load a signal into the analysis object.

#### loadh5(filename=None)

Load data from an HDF5 file into the analysis object.

#### convert_signal(signal, adzero, conversionfactor, exponent)

Convert raw signal values to physical units using the provided parameters:
adzero, conversionfactor, and exponent

#### convert_threshold(threshold, adzero, conversionfactor, exponent)

Convert threshold value to physical units using the provided parameters:
adzero, conversionfactor, and exponent.

#### loadspikes(spikes)

Load spike data into the analysis object.

### loadwell(file:str, well, method = 'default', spikes = False, reverbs = False,

> bursts = False, net_reverbs = False, net_bursts = False)

Load data for a specific well.

#### detect_threshold()

Detect threshold value for spike detection.

#### convert_timestamps_to_binary(input_timestamp, input_type, size=None)

Convert timestamps to binary representation.

#### convert_binary_to_timestamps(input_binary, input_type)

Convert binary representation data to timestamps.

#### \_convert_binary_reverb_to_timestamp(input_binary)

Convert binary representation of reverbs to timestamps.

#### \_detect_spikes(signal, threshold)

Detect spikes in a signal based on a given threshold value.

#### detect_spikes()

Detect spikes in the signal.

#### reverbs_params_default()

Set default parameters for reverberations/bursts detection.

#### reverbs_params_manual(params)

Set manual parameters for reverberations detection.

#### detect_reverbs(method='default')

Detects reverberations based on the specified method.

#### \_detect_reverbs(spikes)

Detect reverberations based on spikes timestamps.

#### detect_bursts()

Detect bursts in the signal based on the detected reverberations.

#### \_detect_bursts(reverbs)

Detect bursts based on reverberations.

#### \_predict_reverbs(model_input, spikes_binary)

Predict reverberations using a pre-trained machine learning model.

#### normalize_signal(signal)

Normalize the input signal.

#### normalize_threshold(signal, threshold)

Normalize the threshold relative to the input signal.

#### reduce_dimension(X, input_type=None, reduction_factor=None)

Reduce the dimensionality of the input data.

#### reduce_norm_abs_signal(signal)

Reduce the dimensionality of the normalized absolute signal.

#### detect_net(inp, minChannelsParticipating=8, minSimultaneousChannels=6)

Detect net bursts or net reverbs.

#### save_spikes()

Save spikes data to a CSV file.

#### analyze_dataset(file=None, mode='csv', save_default=False)

Analyze the datasets from a CSV file,  and save the results to CSV files.

### plot_window(signal, start_time=None, duration=None, threshold=None, spikes=None, reverberations=None,

> bursts=None, net_bursts=None, save=False, output_name=None, figsize=(6, 6), yunits=’a.u.’,
> xunits=’s’)

Plot a window of the signal with detected spikes, reverberations, bursts, and network bursts.

#### plot_raster(spikes, reverbs=None, bursts=None, net_reverbs=None, net_bursts=None)

Plot a raster plot of spikes with optional overlay of reverberations, bursts, and network bursts.

### plot_raster_well(file:str, well, method = 'default', reverbs = False, bursts = False,

> net_reverbs = False, net_bursts = False)

Plot a raster plot for a specific well with optional overlay of events.

Initializes class with default attributes.

#### analyze_dataset(file=None, mode='csv', save_default=False)

Analyze the datasets from a CSV file,  and save the results to CSV files.

This method conducts a thorough analysis of the dataset, encompassing the detection of reverberations (reverbs),
bursts, network bursts, and various statistics. It then saves the results to CSV files based on the specified mode
and whether to save default analysis results.

* **Parameters:**
  * **file** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *or* *None* *,* *optional*) – The file name or path of the CSV with datasets to analyze. If None, the dataset associated with the
    dataset attribute will be analyzed.
  * **mode** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – The mode of analysis. Default is ‘csv’, meaning it will receiva a CSV file with the dataset information.
  * **save_default** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Determines whether to also save the results of default analysis alongside model-based analysis. Default is False.

### Notes

- The method first creates dataframes to store information about reverbs, bursts, network bursts, and statistics.
- It loads the CSV file, iterates over each dataset, and analyzes each well speficified within the dataset.
- For each well, it performs spike detection, threshold detection, and then applies the specified methods
  (default or model-based) for reverbs, bursts, and network bursts detection.
- It calculates various statistics based on the detected bursts and network bursts.
- Finally, it saves the results to separate CSV files according to the specified mode and whether to save default
  analysis results.

#### convert_binary_to_timestamps(input_binary, input_type)

Convert binary representation data to timestamps.

This method converts a binary representation to timestamps for the specified input type,
such as ‘spikes’, ‘reverbs’, or ‘bursts’.

* **Parameters:**
  * **input_binary** (*array_like*) – The binary representation data to be converted to timestamps.
  * **input_type** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – The type of input binary. Options are ‘spikes’, ‘reverbs’, or ‘bursts’.
* **Returns:**
  The timestamps corresponding to the binary representation.
* **Return type:**
  [list](https://docs.python.org/3/library/stdtypes.html#list) or [list](https://docs.python.org/3/library/stdtypes.html#list) of lists

### Examples

```pycon
>>> obj = Analysis()
>>> spikes_timestamps = obj.convert_binary_to_timestamps(spikes_binary, input_type='spikes')
```

### Notes

- For ‘spikes’, each 1 in the binary array represents a spike timestamp.
- For ‘reverbs’ and ‘bursts’, consecutive 1s in the binary array represent timestamp ranges.

#### convert_signal(signal, adzero, conversionfactor, exponent)

Convert raw signal values to physical units using the provided parameters:
adzero, conversionfactor, and exponent

* **Parameters:**
  * **signal** (*array_like*) – The raw signal values to be converted.
  * **adzero** ([*float*](https://docs.python.org/3/library/functions.html#float)) – The adZero value to subract from signal.
  * **conversionfactor** ([*float*](https://docs.python.org/3/library/functions.html#float)) – The conversion factor to convert raw values to physical units.
  * **exponent** ([*float*](https://docs.python.org/3/library/functions.html#float)) – The exponent applied during conversion.
* **Returns:**
  The converted signal values in physical units.
* **Return type:**
  array_like

### Examples

```pycon
>>> obj = Analysis()
>>> converted_signal = obj.convert_signal(raw_signal, ad_zero_value, conversion_factor, exponent_value)
```

#### convert_threshold(threshold, adzero, conversionfactor, exponent)

Convert threshold value to physical units using the provided parameters:
adzero, conversionfactor, and exponent.

* **Parameters:**
  * **threshold** ([*float*](https://docs.python.org/3/library/functions.html#float)) – The threshold value to be converted.
  * **adzero** ([*float*](https://docs.python.org/3/library/functions.html#float)) – The AD Zero value.
  * **conversionfactor** ([*float*](https://docs.python.org/3/library/functions.html#float)) – The conversion factor to convert raw values to physical units.
  * **exponent** ([*float*](https://docs.python.org/3/library/functions.html#float)) – The exponent applied during conversion.
* **Returns:**
  The converted threshold value in physical units.
* **Return type:**
  [float](https://docs.python.org/3/library/functions.html#float)

### Examples

```pycon
>>> obj = Analysis()
>>> converted_threshold = obj.convert_threshold(threshold_value, ad_zero_value, conversion_factor, exponent_value)
```

#### convert_timestamps_to_binary(input_timestamp, input_type, size=None)

Convert timestamps to binary representation.

This method converts timestamps to a binary representation suitable for the specified input type,
such as ‘spikes’, ‘reverbs’, or ‘bursts’.

* **Parameters:**
  * **input_timestamp** (*array_like*) – The timestamps data to be converted to binary representation.
  * **input_type** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – The type of input timestamps. Options are ‘spikes’, ‘reverbs’, or ‘bursts’.
  * **size** ([*int*](https://docs.python.org/3/library/functions.html#int) *,* *optional*) – The size of the binary representation dta. Default is None.
* **Returns:**
  The binary representation of the timestamps data.
* **Return type:**
  array_like

### Examples

```pycon
>>> obj = Analysis()
>>> spikes_binary = obj.convert_timestamps_to_binary(spikes_timestamps, input_type='spikes')
```

### Notes

- If ‘size’ is not provided, it defaults to the total number of timesteps in the signal.
- For ‘spikes’, each spike timestamp is represented as 1 in the binary array.
- For ‘reverbs’ and ‘bursts’, each timestamp range is represented as 1 in the binary array.

#### detect_bursts()

Detect bursts in the signal based on the detected reverberations.

* **Return type:**
  None

### Notes

- If no reverberations are detected or if the ‘reverbs’ attribute is empty, no bursts will be detected.
- Detected bursts are stored in the ‘bursts’ attribute.

#### detect_net(inp, minChannelsParticipating=6, minSimultaneousChannels=3)

Detect net bursts or net reverbs.

* **Parameters:**
  * **inp** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – Input type for which to detect the net (‘reverbs’ or ‘bursts’).
  * **minChannelsParticipating** ([*int*](https://docs.python.org/3/library/functions.html#int) *,* *optional*) – Minimum number of channels participating to start a net burst.
  * **minSimultaneousChannels** ([*int*](https://docs.python.org/3/library/functions.html#int) *,* *optional*) – Minimum number of simultaneous channels to continue a net burst.

### Notes

- A net burst or net reverb is defined by bursts or reverbs occurring simultaneously on multiple channels.
- The function detects net bursts or net reverbs based on the input type.
- It starts a net burst when the number of channels bursting together exceeds ‘minChannelsParticipating’.
- It ends a net burst when the number of simultaneous channels drops below ‘minSimultaneousChannels’.
- The resulting net bursts or net reverbs are stored in the corresponding attributes (‘net_reverbs’ or ‘net_bursts’)
  and their binary representations are stored in ‘net_reverbs_binary’ or ‘net_bursts_binary’, respectively.

#### detect_reverbs(method='default')

Detects reverberations based on the specified method.

* **Parameters:**
  **method** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – The method used for reverberations detection. Options are ‘default’, ‘manual’, or ‘model’. Default is ‘default’.
* **Return type:**
  None

### Notes

- If method is ‘default’, the default parameters for reverberations detection are used.
- If method is ‘manual’, the user can specify manual parameters for reverberations detection.
- If method is ‘model’, reverberations are detected using a pre-trained machine learning model.
- Detect reverberations are stored in the reverb attribute.

#### detect_spikes()

Detect spikes in the signal.

This method detects spikes from the signal attribute of the analysis object based on the threshold attribute.

* **Return type:**
  None

### Notes

- It checks if the signal attribute is a numpy array, and if not, prints an error message and returns.
- If the signal attribute is a 1D numpy array, it detects spikes and updates the spikes and spikes_binary attributes.
- If the signal attribute is a 2D numpy array (multiple channels), it detects spikes for each channel and updates
  the spikes and spikes_binary attributes accordingly.

#### detect_threshold()

Detect threshold value for spike detection.

This method calculates the threshold(s) value(s) based on the signal data and threshold parameters
provided in the analysis object.

* **Return type:**
  None

### Examples

```pycon
>>> obj = Analysis()
>>> obj.detect_threshold()
```

### Notes

- If the ‘signal’ attribute is not a numpy array, an error message is printed, and the method returns.
- The threshold value is calculated segment-wise based on the mean and standard deviation of the signal.
- The calculated threshold value is stored in the ‘threshold’ attribute of the analysis object.

#### files_and_well_csv(file)

Load filenames and associated wells from a CSV file.

This method reads a CSV file containing filenames and associated wells,
and populates the *dataset* and *wellsLabels* attributes accordingly.

* **Parameters:**
  **file** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – The name of the CSV file to load, relative to the path_to_csv attribute.
* **Return type:**
  None

### Examples

```pycon
>>> obj = Analysis()
>>> obj.files_and_well_csv('data.csv')
```

CSV file format example:
filename;wells
file1.h5;A1,B2,C3
file2.h5f;all
file3.h5;D4,E5,F6

In the above example, we want to analyze wells A1, B2, and C3 fromfile1.h5,
all wells from file2.h5, and D4, E5 and F6 from and file3.h5.

#### loadh5(filename=None)

Load data from an HDF5 file into the analysis object.

* **Parameters:**
  **filename** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – The name of the HDF5 file to load. If None, it loads the dataset specified in the dataset attribute.
* **Return type:**
  None

### Notes

- If None, sets the dataset attribute equals to the filename input
- The ‘infoChannel’, ‘adZero’, ‘conversionFactor’, ‘exponent’, and ‘wellsFromData’ attributes are updated
  based on the data loaded from the HDF5 file.

### Examples

```pycon
>>> obj = Analysis()
>>> obj.loadh5()  # Load the dataset from dataset attribute
```

```pycon
>>> obj.loadh5('example.h5')  # Load data from a specific HDF5 file
```

#### loadmodel(setting='default')

Load a machine learning model to perform reverberations/bursts detection.

This method loads a machine learning model specified by *model_name* and updates the
*model_params* attribute based on the chosen setting (‘default’ or ‘manual’). If no
model_name is provided, it loads a default model named ‘signal30.h5’.

* **Parameters:**
  **setting** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – The setting to use for loading the model. Options are ‘default’ or ‘manual’.
* **Return type:**
  None

### Examples

```pycon
>>> obj = Analysis()
>>> obj.loadmodel()  # Load default model with default settings
```

```pycon
>>> obj.loadmodel(setting='manual')  # Load default model with manual settings
```

### Notes

- If *model_name* is already specified, the method updates *model_params* accordingly.
- The default settings include setting *input_average* to 30, *window_size* to 50,000,
  and *window_overlap* to 25,000.
- If a model with the same name as *model_name* is found in  *\_pretrained_models*,
  it sets *input_type* based on the model name and updates other parameters accordingly.

#### loadsignal(signal)

Load a signal into the analysis object.

* **Parameters:**
  **signal** (*array_like*) – The signal data to be loaded for analysis. It can be a 1D or 2D array,
  depending if the signal has one or multiple channels.
* **Return type:**
  None

### Notes

- If the signal is 1D, it is assumed to be a one-channel time series signal.
- If the signal is 2D, it is assumed to be a collection of channels, with signals over time.
- The *total_timesteps_signal* attribute is updated to reflect the length of the signal.
- The [`time`](#id1) attribute is generated based on the sampling frequency and the length of the signal.

### Examples

```pycon
>>> obj = Analysis()
>>> obj.loadsignal(signal_data)
```

#### loadspikes(spikes)

Load spike data into the analysis object.

This method allows loading spike data into the analysis object for further processing.

* **Parameters:**
  **spikes** (*array_like*) – The spike data to be loaded for analysis.
* **Return type:**
  None

#### loadwell(file, well, method='default', spikes=False, reverbs=False, bursts=False, net_reverbs=False, net_bursts=False)

Load data for a specific well.

This method loads data for a specific well from an HDF5 file into the analysis object for further processing.
Optionally, it can also detect spikes and analyze them for reverberations and bursts, calling the deticated methods.

* **Parameters:**
  * **file** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – The name of the HDF5 file containing the data.
  * **well** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *or* [*int*](https://docs.python.org/3/library/functions.html#int)) – The label or index of the well to load.
  * **method** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – The method to use for detecting reverbs and bursts if spikes are detected. Default is ‘default’.
  * **spikes** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to detect spikes. Default is False.
  * **reverbs** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to analyze reverberations. Default is False.
  * **bursts** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to analyze bursts. Default is False.
  * **net_reverbs** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to analyze net reverberations. Default is False.
  * **net_bursts** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to analyze net bursts. Default is False.
* **Return type:**
  None

### Examples

```pycon
>>> obj = Analysis()
>>> obj.loadwell('data.h5', 'well_label', method='model', spikes=True, reverbs=True)
```

### Notes

- The *file* parameter specifies the name of the HDF5 file containing the data.
- The *well* parameter can be either a well label (str) or index (int).
- The *method* parameter determines the method to use for detecting reverberations and bursts.
  It is only used if spikes are detected and defaults to ‘default’.
- The *spikes*, *reverbs*, *bursts*, *net_reverbs*, and *net_bursts* parameters control which
  analyses to perform after loading the data. They default to False.

#### normalize_signal(signal)

Normalize the input signal.

* **Parameters:**
  **signal** (*array_like*) – Input signal to be normalized.
* **Returns:**
  Normalized signal.
* **Return type:**
  array_like

### Notes

- The normalization is performed by dividing the signal by its maximum absolute value.

#### normalize_threshold(signal, threshold)

Normalize the threshold relative to the input signal.

* **Parameters:**
  * **signal** (*array_like*) – Input signal used for normalization.
  * **threshold** ([*float*](https://docs.python.org/3/library/functions.html#float)) – Threshold value to be normalized.
* **Returns:**
  Normalized threshold.
* **Return type:**
  [float](https://docs.python.org/3/library/functions.html#float)

### Notes

- The threshold is normalized by dividing it by the maximum absolute value of the input signal.

#### plot_raster(spikes, reverbs=None, bursts=None, net_reverbs=None, net_bursts=None, start_time=None, end_time=None, save=None, show=True)

Plot a raster plot of spikes with optional overlay of reverberations, bursts, and network bursts.

* **Parameters:**
  * **spikes** (*array_like*) – The timestamps of spikes. Can be a list of timestamps for multiple channels.
  * **reverbs** (*array_like* *,* *optional*) – The timestamps data of detected reverberations.
  * **bursts** (*array_like* *,* *optional*) – The timestamps data of detected bursts.
  * **net_reverbs** (*array_like* *,* *optional*) – The timestamps data of detected network reverberations.
  * **net_bursts** (*array_like* *,* *optional*) – The timestamps data of detected network bursts.
  * **start_time** ([*float*](https://docs.python.org/3/library/functions.html#float) *,* *optional*) – Starting time of the plot, in seconds.
  * **end_time** ([*float*](https://docs.python.org/3/library/functions.html#float) *,* *optional*) – Ending time of the plot, in seconds.
  * **save** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – Name of the generated plot figure.
  * **show** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to show or not the plot. Default is True.
* **Return type:**
  None

### Notes

- Each spike is represented by a vertical line at its timestamp.
- Detected events are filled between their start and end timestamps.

#### plot_raster_well(file, well, method='default', reverbs=False, bursts=False, net_reverbs=False, net_bursts=False, start_time=None, end_time=None, save=None)

Plot a raster plot for a specific well with optional overlay of events.

* **Parameters:**
  * **file** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – The filename or path of the data file.
  * **well** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – The label or ID of the well to plot.
  * **method** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – The method used for detecting events. Default is ‘default’.
  * **reverbs** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to overlay detected reverberations on the raster plot. Default is False.
  * **bursts** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to overlay detected bursts on the raster plot. Default is False.
  * **net_reverbs** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to overlay detected network reverberations on the raster plot. Default is False.
  * **net_bursts** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to overlay detected network bursts on the raster plot. Default is False.
  * **start_time** ([*float*](https://docs.python.org/3/library/functions.html#float) *,* *optional*) – Starting time of the plot, in seconds.
  * **end_time** ([*float*](https://docs.python.org/3/library/functions.html#float) *,* *optional*) – Ending time of the plot, in seconds.
  * **save** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – Name of the generated plot figure.
* **Return type:**
  None

#### plot_window(signal, start_time=None, duration=None, threshold=None, spikes=None, reverberations=None, net_reverberations=None, bursts=None, net_bursts=None, save=None, show=True, figsize=(6, 6), yunits='a.u.', xunits='s')

Plot a window of the signal with detected spikes, reverberations, bursts, and network bursts.

* **Parameters:**
  * **signal** (*array_like*) – The input signal data.
  * **start_time** ([*float*](https://docs.python.org/3/library/functions.html#float) *,* *optional*) – The start time of the window in seconds. Default is None (start from the beginning).
  * **duration** ([*float*](https://docs.python.org/3/library/functions.html#float) *,* *optional*) – The duration of the window in seconds. Default is None (plot until the end of the signal).
  * **threshold** ([*float*](https://docs.python.org/3/library/functions.html#float) *,* *optional*) – The threshold value for plotting. Default is None (no threshold line).
  * **spikes** (*array_like* *,* *optional*) – The timestamps of detected spikes. Default is None (no spikes in the plot)
  * **reverberations** (*array_like* *,* *optional*) – The timestamps data of detected reverberations. Default is None
  * **bursts** (*array_like* *,* *optional*) – The timestamps data detected bursts. Default is None
  * **net_bursts** (*array_like* *,* *optional*) – The timestamps data of detected network bursts. Default is None
  * **save** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – Name of the generated plot figure.
  * **show** ([*bool*](https://docs.python.org/3/library/functions.html#bool) *,* *optional*) – Whether to show or not the plot. Default is True.
  * **figsize** ([*tuple*](https://docs.python.org/3/library/stdtypes.html#tuple) *,* *optional*) – The size of the figure (width, height) in inches. Default is (6, 6).
  * **yunits** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – The units of the y-axis. Default is ‘a.u.’ (arbitrary units).
  * **xunits** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – The units of the x-axis. Default is ‘s’ (seconds).

### Notes

- It normalizes the signal if yunits is ‘a.u.’ and converts it to millivolts (mV) if yunits is ‘mV’.
- Detected spikes, reverberations, bursts, and network bursts are overlaid on the plot with different colors.

#### reduce_dimension(X, input_type=None, reduction_factor=None)

Reduce the dimensionality of the input data.

* **Parameters:**
  * **X** (*array_like*) – Input data to be dimensionally reduced.
  * **input_type** ([*str*](https://docs.python.org/3/library/stdtypes.html#str) *,* *optional*) – Type of input data (‘signal’ or ‘spikes’).
  * **reduction_factor** ([*int*](https://docs.python.org/3/library/functions.html#int) *,* *optional*) – Factor by which to reduce the dimensionality.
* **Returns:**
  Dimensionally reduced input data.
* **Return type:**
  array_like

### Notes

- If ‘input_type’ is ‘spikes’, the dimensionality is reduced by selecting every ‘reduction_factor’ element.
- If ‘input_type’ is ‘signal’, the dimensionality is reduced by averaging every ‘reduction_factor’ elements.

#### reduce_norm_abs_signal(signal)

Reduce the dimensionality of the normalized absolute signal.

* **Parameters:**
  **signal** (*array_like*) – Input signal to be normalized and dimensionally reduced.
* **Returns:**
  Dimensionally reduced normalized absolute signal.
* **Return type:**
  array_like

#### reverbs_params_default()

Set default parameters for reverberations/bursts detection.

* **Return type:**
  None

### Notes

- This method sets default parameters for detecting reverberations, including:
  - max_interval_start: Maximum interval start time for a reverberation.
  - max_interval_end: Maximum interval end time for a reverberation.
  - min_interval_between: Minimum interval between reverberations.
  - min_duration: Minimum duration of a reverberation.
  - min_spikes: Minimum number of spikes required to consider a reverberation.

#### reverbs_params_manual(params)

Set manual parameters for reverberations detection.

* **Parameters:**
  **params** ([*list*](https://docs.python.org/3/library/stdtypes.html#list)) – List containing manual parameters for reverberations detection in the following order:
  - max_interval_start: Maximum interval start time for a reverberation.
  - max_interval_end: Maximum interval end time for a reverberation.
  - min_interval_between: Minimum interval between reverberations.
  - min_duration: Minimum duration of a reverberation.
  - min_spikes: Minimum number of spikes required to consider a reverberation.
* **Return type:**
  None

#### save_spikes()

Save spikes data to a CSV file.

### Notes

- The method saves spikes data to a CSV file containing columns for channel ID, channel label, well ID, well label, compound ID, compound name,
  experiment, dose in pM, dose label, and timestamp.
- The CSV file is saved in the output folder with a filename based on the output name attribute and the type of data being saved
  (e.g., ‘_REVERBS_PREDICTED.csv’).
