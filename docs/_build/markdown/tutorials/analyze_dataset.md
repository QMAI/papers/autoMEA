# Analyze dataset

The easiest way to use *automea* is to set up a *csv* file with a list of datasets and corresponding wells we want to analyze. The method *analyze_dataset()* takes as input a csv file and, based on parameters defined by the user, saves different output like statistics for each dataset, a list of spikes and bursts for each well, and more.

As an explample we have prepared the file ‘filenames_and_wells.csv’. Let’s load it with *pandas* to take a look at it.

```python
import pandas as pd
csv_file = '../automea/final/filenames_and_wells_2.csv'
filenames_and_wells = pd.read_csv(csv_file, sep = ';')
filenames_and_wells
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }
```none
.dataframe tbody tr th {
    vertical-align: top;
}

.dataframe thead th {
    text-align: right;
}
```

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>filename</th>
      <th>wells</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>AH22C001_3039_DIV14.h5</td>
      <td>A6</td>
    </tr>
  </tbody>
</table>
</div>

The file contain the dataset named ‘AH22C001_3039_DIV14.h5’, and one well (A6).

```python
import os
os.chdir('../automea/final/')
```

To perform the analysis we first need to define a set of parameters. Let’s start by importing *automea* and creating an object from the *Analysis()* class.

```python
import automea_copy
```

```python
am = automea_copy.Analysis()
```

One of the user defined parameters is which model one wants to use to perform the analysis - the machine learning models used for burst detection.

The user needs to define both the *path_to_model* attribute indicating the folder containing the model, and the *model_name*. After this the method *loadmodel()* can be called, and the chosen model will be user for following analyses.

```python
am.path_to_model = ''#'models/'
am.model_name = 'signal30.h5'
am.loadmodel()
```

```none
Metal device set to: Apple M1
```

We can check the model parameters, used for burst detection, by looking the at *model_params* attribute.

```python
am.model_params
```

```none
{'name': 'signal30.h5',
 'input_type': 'signal',
 'input_average': 30,
 'window_size': 50000,
 'window_overlap': 25000}
```

With the model loaded, we can define what we can to save from the analysis, by changing the *analysis_params* attribute.

```python
am.analysis_params
```

```none
{'save_spikes': False,
 'save_reverbs': False,
 'save_bursts': False,
 'save_net_reverbs': False,
 'save_net_bursts': False,
 'save_stats': False}
```

For instance, if we want to save only the high-level statistics, we set

```python
am.analysis_params['save_stats'] = True
```

Now, we need to indicate where to find the csv file containing the datasets we want to analyze, the location of the datasets, and define a name for the output files.

```python
am.path_to_csv = ''
csv_file = 'filenames_and_wells_2.csv'
am.path_to_dataset = '../../qneuron/mea-reproduce-results/mea-h5-datafiles/'
am.output_name = 'test'

```

Finally, we can run the *analyze_dataset* function for a csv file.

```python
am.analyze_dataset(csv_file)
```

```none
--- Running full analysis ---

--- Using model  signal30.h5  ---


 Analyzing dataset:  AH22C001_3039_DIV14.h5 

Well:  A6

--- Done! ---
```

While the code is running, a message shows which datasets and well are currently being analyzed.

After some minutes - or hours depending on how many datasets we want to analyze - a *Done!* message is shows indidcating that the process is finished.

The statistics we saved can be found in the file named ‘test_STATS_PREDICTED.csv’. We can import the file and look at the results.

```python
stats = pd.read_csv('test_STATS_PREDICTED.csv')
stats
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }
```none
.dataframe tbody tr th {
    vertical-align: top;
}

.dataframe thead th {
    text-align: right;
}
```

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Filename</th>
      <th>Well Label</th>
      <th>Number of channels</th>
      <th>Total number of spikes</th>
      <th>Mean Firing Rate [Hz]</th>
      <th>Stray spikes (%)</th>
      <th>Total number of networks bursts</th>
      <th>Mean Network Bursting Rate [bursts/minute]</th>
      <th>Mean Network Burst Duration [ms]</th>
      <th>NIBI</th>
      <th>CV of NIBI</th>
      <th>Mean reverb per burst</th>
      <th>Median of reverb per burst</th>
      <th>Mean net reverb per net burst</th>
      <th>Median of net reverb per net burst</th>
      <th>Total number of network reverb</th>
      <th>Mean net reverb frequency [reverb/min]</th>
      <th>Mean net reverb duration [ms]</th>
      <th>Mean in-reverb freq [Hz]</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>AH22C001_3039_DIV14.h5</td>
      <td>A6</td>
      <td>12</td>
      <td>56193</td>
      <td>7.8</td>
      <td>11.92</td>
      <td>49</td>
      <td>4.9</td>
      <td>759.41</td>
      <td>11199.72</td>
      <td>0.27</td>
      <td>4.84</td>
      <td>5.0</td>
      <td>4.73</td>
      <td>5.0</td>
      <td>237</td>
      <td>23.7</td>
      <td>91.77</td>
      <td>189.31</td>
    </tr>
  </tbody>
</table>
</div>

The file contains statistics about the dataset/well analyzed.

```python

```

```python

```

```python

```
